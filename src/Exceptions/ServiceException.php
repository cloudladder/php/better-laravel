<?php

declare(strict_types = 1);

namespace Gupo\BetterLaravel\Exceptions;

use Gupo\BetterLaravel\Exceptions\Traits\RenderHttpResponse;

/**
 * 服务层 异常
 */
class ServiceException extends \Exception implements \Throwable
{
    use RenderHttpResponse;
}
