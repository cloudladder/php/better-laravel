<?php

declare(strict_types = 1);

namespace Gupo\BetterLaravel\Exceptions;

use Gupo\BetterLaravel\Exceptions\Traits\RenderHttpResponse;

/**
 * 接口 异常
 */
class ApiException extends \Exception implements \Throwable
{
    use RenderHttpResponse;
}
