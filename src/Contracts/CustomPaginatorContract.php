<?php

declare(strict_types = 1);

namespace Gupo\BetterLaravel\Contracts;

interface CustomPaginatorContract
{
    /**
     * @return array
     */
    public function toArray(): array;

    /**
     * 构建空数据分页对象
     *
     * @param bool $fetchAll 标识是否取回所有数据
     *
     * @return self
     */
    public static function toEmpty(bool $fetchAll = false): self;

    /**
     * 解析出分页对象
     *
     * @param mixed $items
     * @param null|int $perPage
     * @param null|int $page
     * @param bool $fetchAll 标识是否取回所有数据
     * @return self
     */
    public static function resolve(
        mixed $items,
        ?int $perPage = null,
        ?int $page = null,
        bool $fetchAll = false
    ): self;
}
