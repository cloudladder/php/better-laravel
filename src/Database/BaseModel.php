<?php

declare(strict_types = 1);

namespace Gupo\BetterLaravel\Database;

use Illuminate\Database\Eloquent\Model;
use Gupo\BetterLaravel\Database\Traits\IdeHelpers;
use Gupo\BetterLaravel\Database\Traits\SerializeDate;

/**
 * 模型基类
 */
abstract class BaseModel extends Model
{
    use SerializeDate, IdeHelpers;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = ['deleted_at'];
}
