<?php

declare(strict_types = 1);

namespace Gupo\BetterLaravel\Rules;

use Illuminate\Contracts\Validation\Rule;
use Gupo\BetterLaravel\Rules\Traits\NormalMessageFormat;

/**
 * 验证规则基类
 */
abstract class BaseRule implements Rule
{
    use NormalMessageFormat;
}
