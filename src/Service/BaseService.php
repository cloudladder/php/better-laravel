<?php

declare(strict_types = 1);

namespace Gupo\BetterLaravel\Service;

use Gupo\BetterLaravel\Traits\InstanceMake;

/**
 * 服务层基类
 */
abstract class BaseService
{
    use InstanceMake;
}
